import {combineReducers} from 'redux'
import searchReducer from './search/searchReducer'
import {persistStore, persistReducer} from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage';

const persistConfig = {
    key: 'searchReducer',
    storage: AsyncStorage,  
    whitelist: ['searching'],
};

const rootReducer = combineReducers({
    searchReducer: persistReducer(persistConfig, searchReducer),
})
export default rootReducer