import {createStore,applyMiddleware} from 'redux'

import rootReducer from './rootReducer'
import thunk from 'redux-thunk'
import {persistStore, persistReducer} from 'redux-persist';
const middleware=[thunk];
const store = createStore(rootReducer,applyMiddleware(...middleware))
export let persistor = persistStore(store);
export default store