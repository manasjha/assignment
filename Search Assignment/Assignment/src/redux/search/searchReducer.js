
import { REHYDRATE } from 'redux-persist/es/constants';
import{PREVIOUS_SEARCH} from './searchTypes'

const initialState = {
    searching:[]
}

const searchReducer = ( state = initialState,action)=>{
    console.log("Action=>",action);
    console.log("Current State",state);
    switch(action.type){
        case PREVIOUS_SEARCH : return{
            ...state,
            searching:[...action.payload]
        }

        case REHYDRATE:return {
            ...state,
        }

        default: return state
    }
    



}
export default searchReducer