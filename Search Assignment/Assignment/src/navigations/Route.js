import React from 'react'
import {View} from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import search from '../container/screen/App/search';
import searchDetails from '../container/screen/App/searchDetails';

const Stack = createStackNavigator();
export default function Route() {
    return (
        <NavigationContainer>
        <Stack.Navigator screenOptions={{headerShown: false}} initialRouteName="search">
        <Stack.Screen name="searchDetails" component={searchDetails} />
        <Stack.Screen name="search" component={search} />
        
       
    </Stack.Navigator>
    </NavigationContainer>
    )
}
