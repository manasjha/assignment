import React from 'react';
import {Image, TextInput, View, StyleSheet,ActivityIndicator,TouchableOpacity} from 'react-native';
import { colors } from '../../constants/colors';
import { ImageUrlConstants } from '../../constants/imageUrl';
import { heightPercentageToDP as hp , widthPercentageToDP as wp} from '../../helper/responsiveScreen';

const SearchBar = props => {
  return (
    <TouchableOpacity activeOpacity={1} style={[Style.searchBar, props.style]} onPress={props.onPressSearch}>
    <Image source={ImageUrlConstants.search}  style={{width:hp('3%'),height:hp('3%'),marginLeft:wp('2%')}}/> 
    <TextInput
        maxLength={30}
        style={Style.textInput}
        placeholderTextColor={colors.secondary}
        placeholderStyle={Style.textInput}
        onChangeText={props.onChangeText}
        placeholder="Search..."
        value={props.value}
        ref={props.ref} 
        autoFocus={props.autoFocus}
        autoFocus = {true}
         onFocus={props.onFocus}
         blurOnSubmit={props.blurOnSubmit}
         onBlur={props.onBlur}
         returnKeyType={props.returnKeyType}
         onSubmitEditing={props.onSubmitEditing}
         editable={props.editable}
      
      />
     
     
    </TouchableOpacity>
  );
};

const Style = StyleSheet.create({
  searchBar: {
    backgroundColor: '#fff',
    borderRadius: 5,
    borderWidth: 1.5,
    borderColor: colors.lightGrey,
    flexDirection: 'row',
    paddingVertical: hp('1%'),
    paddingHorizontal: wp('3%'),
    justifyContent: 'space-between',
    alignItems: 'center',
  },
    textInput:{
        fontFamily: 'Poppins-Regular',
        color:"#000",
        height: 40,
        borderColor: colors.secondary,
        borderWidth: 0,
        flex: 1,
        marginTop:5,
        fontSize: hp('1.6%'),
    }
});
export default SearchBar;
