import React, { useState, useEffect, useRef } from 'react'
import { View, ScrollView, Modal, StyleSheet, FlatList, Text, TouchableOpacity, Image, TouchableHighlight } from 'react-native'
import SearchBar from '../../component/SearchBar'
import { post, get } from '../../../services/post';
import { searching } from '../../../redux'
import { useSelector, useDispatch } from 'react-redux'
import { PREVIOUS_SEARCH } from '../../../redux/search/searchTypes'
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from '../../../helper/responsiveScreen';
let arr = []
let previous = []
export default function search() {
    const [result, setresult] = useState([])
    const [searchresult, setsearch] = useState(false)
    const [value, setvalue] = useState('')
    const [modalVisible, setModalVisible] = useState(false);
    const [view, setView] = useState('grid');
    const state = useSelector(state => state.searchReducer.searching)
    previous = state
    const serachref = useRef('');
    const dispatch = useDispatch()

    useEffect(() => {

    }, [])

    async function search(term) {

        setsearch(false)

        await get('search?term=' + term).then(data => handleResponse(data))
        await arr.push(term)
        await dispatch(searching(PREVIOUS_SEARCH, arr))
        previous = [...new Set(arr)]


    }
    const handleResponse = (data) => {

        console.log(data.results)
        setresult(data.results)
        setModalVisible(!modalVisible)

    }
    const renderItem = ({ item, index }) => {

        return (
            <View style={{
                justifyContent: 'center', alignItems: 'center',
                flexDirection: view === 'grid' ? 'column' : 'row',
                width: view === 'grid' ? wp('45') : wp('90'), padding: 10
            }}>

                <Image style={styles.image} source={{ uri: item.artworkUrl100 }} />
                <Text style={styles.artist}>{item.primaryGenreName}</Text>
            </View>
        );
    };
    function previousSearch(item) {

        setvalue(item)
        search(item)
    }
    const searchItem = ({ item, index }) => {
        return (
            <View style={styles.searchConatiner}>
                <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => previousSearch(item)}>
                    <Text>{item}</Text>
                </TouchableOpacity>

            </View>
        );

    }
    return (
        <View style={styles.container}>

            <SearchBar
                onChangeText={text => setvalue(text)}
                onPressSearch={() => setModalVisible(true)}
                editable={false} value={value}
                onSubmitEditing={() => search(value)}
                returnKeyType="search" />

            <TouchableOpacity style={styles.gridButton} onPress={() => setView(view === 'grid' ? 'list' : 'grid')}>
                <Text style={{ fontSize: 20 }}>{view}</Text>
            </TouchableOpacity>
            <FlatList
                data={result}
                extraData={result}
                renderItem={renderItem}
                numColumns={2}
                keyExtractor={(item, index) => index.toString()}
                // ItemSeparatorComponent={Itemseprator}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}

                columnWrapperStyle={{
                    flexWrap: 'wrap',
                    flex: 1,
                    marginTop: 5,

                }}
            />
            <Modal
                animationType="fade"
                transparent={false}
                visible={modalVisible}
                onRequestClose={() => {

                    setModalVisible(!modalVisible);
                }}
            >
                <View style={{
                    marginTop: hp('2%'),
                    padding: hp('2%')
                }}>
                    <SearchBar
                        onChangeText={text => setvalue(text)}
                        ref={serachref}
                        value={value}
                        onSubmitEditing={() => search(value)}
                        returnKeyType="search" />
                    <FlatList
                        data={previous}
                        extraData={previous}
                        renderItem={searchItem}
                        numColumns={1}
                        keyExtractor={(item, index) => index.toString()}
                        // ItemSeparatorComponent={Itemseprator}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}

                    />
                </View>

            </Modal>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: hp('2%')
    },
    searchConatiner: {
        flex: 1,
        padding: hp('1%'),
        borderWidth: 0.5,
        borderColor: 'grey',

    },
    artist: {
        textAlign: 'center',
        width: 100,
        height: 50
    },
    image: {
        height: 100,
        width: '50%',
        resizeMode: 'cover',
        backgroundColor: 'green'
    },
    gridButton: {
        height: hp('5%'),
        width: hp('10%'),
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1
    },
    cpllection: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        padding: hp('3%')
    }
})
