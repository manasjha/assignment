export const colors ={
    headerBackground : "rgb(79,228,78)",
    grey: "rgb(141,147,149)",
    lightGrey: 'rgb(181,181,189)',
    primary: 'rgb(255,255,255)',
    secondary: 'rgb(25,31,42)',
    primaryText: 'rgb(181,181,189)',
    
    separatorColor: "rgb(245,245,245)",
    disabled: 'rgb(213,213,213)'
} 