import React from 'react'
import { View, Text } from 'react-native'
import SearchDetails from './src/container/screen/App/searchDetails'
import Route from './src/navigations/Route'

import {Provider} from 'react-redux'
import store, { persistor } from './src/redux/store'
import { PersistGate } from 'redux-persist/integration/react'
export default function App() {
  return (
    <Provider store={store} >
      <PersistGate loading={null} persistor={persistor}>
  <Route/>
  </PersistGate>
  </Provider>
 // <SearchDetails/>
  )
}
