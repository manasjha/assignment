function xyz (h,m){
    let hourangle = 0.5 * (h * 60 + m); 
    let minute_angle = (h*60 + m)*6; 
 
    // the difference between two angles 
    let angle = Math.abs(hourangle - minute_angle); 
 
    //  smaller angle of two possible angles 
    angle = Math.min(360 - angle,angle); 
     console.log(angle)
    return angle; 
} 

xyz(4,13)